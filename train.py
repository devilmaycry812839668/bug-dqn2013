from tqdm import trange

from configuration.args_config import args
from environment.atari_env import ALE      # Environment Class
from agent import Agent                   # Agent Class
from memory import Replay                  # Replay Class
from evaluate import evaluate_performance, save_model  


env = ALE()      # env
agent = Agent()  # agent
replay_buffer = Replay()   # Replay


eps_decay = (args.eps_start - args.eps_final) / args.anneal_steps  # type: ignore
epsilon = args.eps_start  

maxQ_states = None

state, done = env.reset(), False


for T in trange(1, args.T_max + 1):
    if T >= args.learn_start:
        action = agent.act_e_greedy(state, epsilon)
    else:
        action = agent.act_e_greedy(state, 1.0)

    next_state, reward, done = env.step(action)

    replay_buffer.append(state[-1], action, reward, done)  # append last screen frame
    state = next_state
    if done:
        state, done = env.reset(), False

    epsilon = max(epsilon - eps_decay, args.eps_final)

    if T >= args.learn_start:  
        # maxQ states
        if maxQ_states is None:
            maxQ_states = replay_buffer.sample(args.fixed_states_size)[0]

        # Update
        if T % args.learn_frequency == 0:
            batch = replay_buffer.sample(args.batch_size)
            agent.learn(batch)
        # Evaluation
        if T % args.evaluation_interval == 0:
            evaluate_performance(T, agent, maxQ_states)

# Save model
save_model(agent)
