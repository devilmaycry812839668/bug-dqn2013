import pickle
import os
import time
import logging
from tqdm import trange
import matplotlib.pyplot as plt

from configuration.args_config import args
from environment.atari_env import ALE
from agent import Agent


metrics = {
    'steps': [],
    'mean_episode_return': [],
    'mean_maxQ': [],
    'best_mean_episode_return': -float('inf')
}


# Test performance over steps
def get_metrics_per_evaluation(agent: Agent, maxQ_states):
    env = ALE()
    All_episodes_rewards = []
    state, single_episode_reward_sum, done = env.reset(), 0, False

    if args.render:
        env.render()
        time.sleep(0.05)

    for _ in trange(args.steps_per_evaluation):
        action = agent.act_e_greedy(state)
        state, reward, done = env.step(action)
        single_episode_reward_sum += reward

        if args.render:
            env.render()
            time.sleep(0.05)
        if done:
            All_episodes_rewards.append(single_episode_reward_sum)
            state, single_episode_reward_sum, done = env.reset(), 0, False

            if args.render:
                env.render()
                time.sleep(0.05)

    if args.render:
        env.close()

    mean_episode_rewards = sum(All_episodes_rewards) / max(1, len(All_episodes_rewards))
    mean_maxQ = agent.evaluate_mean_maxQ_value(maxQ_states)

    return mean_episode_rewards, mean_maxQ


def evaluate_performance(T, agent, val_states):
    mean_episode_rewards, mean_maxQ = get_metrics_per_evaluation(agent, val_states)

    metrics['steps'].append(T)
    metrics['mean_episode_return'].append(mean_episode_rewards)
    metrics['mean_maxQ'].append(mean_maxQ)
    if mean_episode_rewards > metrics['best_mean_episode_return']:
        metrics['best_mean_episode_return'] = mean_episode_rewards
        agent.save(path=args.path, number=T)

    result_log = '| Num of evaluation: ' \
                 + str((T-args.learn_start) // args.evaluation_interval) \
                 + ' | Step: ' \
                 + str(T) \
                 + ' | mean episode return: ' + str(round(mean_episode_rewards, 4)) \
                 + ' | mean maxQ: ' + str(round(mean_maxQ, 4))
    logging.info(result_log)


    plt.figure()  
    plt.plot(metrics['steps'], metrics['mean_episode_return'], marker='.')
    plt.title('mean_episode_return on {}'.format(args.game))
    plt.xlabel('Training Steps')
    plt.ylabel('Mean Episode Return')
    plt.savefig(args.path + '/Mean Episode Return.pdf')
    plt.close()


    plt.figure()
    plt.plot(metrics['steps'], metrics['mean_maxQ'], marker='.')
    plt.title('Mean maxQ on {}'.format(args.game))
    plt.xlabel('Training Steps')
    plt.ylabel('Mean maxQ')
    plt.savefig(args.path + '/Mean maxQ.pdf')
    plt.close()


def save_model(agent):
    agent.save(path=args.path, number='final')
    logging.info('best mean episode return = %f' % metrics['best_mean_episode_return'])

    file = open(os.path.join(args.path, 'metrics.pt'), "wb")
    pickle.dump(metrics, file)
    file.close()

