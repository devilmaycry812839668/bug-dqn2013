from tqdm import trange
import numpy as np
import time
import cv2
import torch

from configuration.args_config import args
from agent import Agent
from environment.atari_env import ALE

# Configuration for log format&file, seed, torch
args.render = True
args.debug = True

data_path = './'  # 该路径上的所有文件夹必须已存在，否则不recording video
fps = 30 #0.03333s    fps=20  0.05s
size = (160, 210)
video = cv2.VideoWriter(f"{data_path+args.game}.avi", cv2.VideoWriter_fourcc(*'XVID'), fps, size)
print(f"{data_path+args.game}.avi")


class ENV(ALE):
    def step(self, action):
        reward, done = 0, False
        frame_buffer = np.zeros((2, 84, 84), dtype=np.uint8)

        for t in range(self.action_repeat):
            reward += self.ale.act(self.actions[action])
            frame_buffer[t % 2] = self._resize_state()
            done = self.ale.game_over()

            lives = self.ale.lives()
            if lives < self.lives:
                if lives > 0:
                    self.life_termination = True
                done = True
            if done:
                break

        observation = frame_buffer.max(0)
        self.state_buffer.append(observation)

        if reward != 0:
            print("reward: ", reward, "lives: ", lives, " done: ", done)
            reward = reward // abs(reward)

        return np.stack(self.state_buffer), reward, done


env = ENV()
agent = Agent()     # Agent
dqn = agent.dqn
"pth_path 是需要手动修改路径的"
pth_path = "/home/devil/PycharmProjects/bug-dqn2013/exp/breakout/957890/2022_07_04-10_50_59/final_model.pth"
dqn.load_state_dict(torch.load(pth_path))


T_rewards = []
state, reward_sum, done = env.reset(), 0, False

ts = 0.00  # 0.01

for step in range(args.steps_per_evaluation):
    if args.render:
        env.render()
        img = env.ale.getScreenRGB()[:, :, ::-1]
        video.write(img)
        time.sleep(ts)

    action = agent.act_e_greedy(state)
    state, reward, done = env.step(action)
    reward_sum += reward

    if done:
        print("reward_sum: ", reward_sum, " steps: ", step,  "  lives: ", env.ale.lives())
        T_rewards.append(reward_sum)
        state, reward_sum, done = env.reset(), 0, False


avg_reward = sum(T_rewards) / max(1, len(T_rewards))
print(avg_reward)

if args.render:
    env.close()
video.release()
