from torch import nn
from torch.nn import functional as F


class DQN(nn.Module):
    def __init__(self, args):
        super(DQN, self).__init__()

        self.convs = nn.Sequential(
            nn.Conv2d(args.history_length, 16, 8, 4),
            nn.ReLU(),
            nn.Conv2d(16, 32, 4, 2),
            nn.ReLU())
        self.conv_output_size = 32 * 9 * 9

        self.fc_h = nn.Linear(self.conv_output_size, args.hidden_size)
        self.fc_z = nn.Linear(args.hidden_size, args.action_space)

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self.conv_output_size)

        q = self.fc_z(F.relu(self.fc_h(x)))

        return q
