import os
import torch

from configuration.args_config import args
from backend.model import DQN


def get_dqn_model():
    return DQN(args).to(device=args.device)


def get_loss_fun():
    return torch.nn.HuberLoss()


def get_optimizer_fun(_model):
    return torch.optim.RMSprop(_model.parameters(), lr=args.lr, alpha=0.95, centered=True)


def update_gradients(loss, optimizer):
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()


def get_maxQ_action_index(_model, _state):         # single state
    state = torch.tensor(_state, device=args.device, dtype=torch.float32).div_(255)

    with torch.no_grad():
        actions = _model(state.unsqueeze(0))
        return actions.max(1)[1].item()  # return max index


def get_actQ_value(_model, _states, actions):        # vector states
    states = torch.tensor(_states, device=args.device, dtype=torch.float32).div_(255)

    return _model(states)[range(states.shape[0]), actions]  # corresponding Q_value for the action


def get_maxQ_value(_model, _states):                 # vector states
    states = torch.tensor(_states, device=args.device, dtype=torch.float32).div_(255)

    with torch.no_grad():
        q = _model(states)
        q_max = q.max(1)[0]
    return q_max.detach_()                   # corresponding max Q_value for the action


def get_target_Q_value(rewards, nonterminals, discount, q_next):
    rewards = torch.tensor(rewards, dtype=torch.float32, device=args.device)
    nonterminals = torch.tensor(nonterminals, dtype=torch.int32, device=args.device)
    discount = torch.tensor(discount, dtype=torch.float32, device=args.device)

    q_target = rewards + nonterminals * discount * q_next
    return q_target


def get_mean_maxQ_value(_model, _states):
    states = torch.tensor(_states, device=args.device, dtype=torch.float32).div_(255)

    with torch.no_grad():
        q_value = _model(states)
        q_mean = q_value.max(1)[0].mean().item()  # return mean of max value
    return q_mean


def save_model(_model, path, name):
    torch.save(_model.state_dict(), os.path.join(path, name))
