# 说明：
该代码只是学习复现而做，代码中存在一定问题，很多细节并没有复现，只是实现了原始论文中的核心功能，因此该代码并不能得到原始论文中的结果，这个代码项目只是为了记录自己学习过程而已，该项目代码不具备follow的价值。




# Environment Setup
```
conda create -n DQN2013 python=3.9
conda install matplotlib
conda install -c conda-forge ipykernel -y
conda install pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch
pip install atari-py
pip install opencv-python
pip install autorom
AutoROM
python -m atari_py.import_roms
pip uninstall autorom
```
需要注意的是要将AutoROM中的rom文件拷贝到atari-py的rom文件夹下

# Current Version
```
python(3.9.7)
matplotlib(3.5.0)
ipykernel(6.6.0)
pytorch(1.10.0)
atari-py(0.2.9)
opencv-python(4.5.4.60)
autorom(0.4.2)
```

# Run Code
```
python train.py --game seaquest
```
