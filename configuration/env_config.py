import atari_py

from configuration.args_config import args


def set_args_action_space():
    ale = atari_py.ALEInterface()  # type: ignore
    ale.loadROM(atari_py.get_game_path(args.game))  # type: ignore

    args.action_space = len(ale.getMinimalActionSet())  # type: ignore


set_args_action_space()




# from configuration.args_config import args
# from environment.atari_env import ALE

# args.action_space = ALE().action_space


