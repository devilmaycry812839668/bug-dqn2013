import os
import sys
import time
import logging

from configuration.args_config import args


def config_log_format():
    # Configuration for files and dirs
    path = '{}/{}/{}/{}'.format(args.exp_name, args.game, args.seed, time.strftime("%Y_%m_%d-%H_%M_%S"))
    args.path = path

    if not os.path.exists(path):
        os.makedirs(path)

    log_format = '%(levelname)s_%(asctime)s %(message)s'
    logging.basicConfig(stream=sys.stdout,
                        level=logging.INFO,
                        format=log_format,
                        datefmt='%H:%M:%S')

    file_log_format = logging.FileHandler(os.path.join(path, 'log.txt'))
    file_log_format.setFormatter(logging.Formatter(fmt=log_format, datefmt='%H:%M:%S'))
    logging.getLogger().addHandler(file_log_format)


def output_args_details():
    """ log config parameters """
    log_str = "\n"
    for i, j in args.__dict__.items():
        log_str += i + " = " + str(j) + "\n"
    logging.info('config_parameter:%s', log_str)


# 设置log格式和环境
config_log_format()

# 打印args参数细节
output_args_details()
