import torch
import random
import numpy as np

from configuration.args_config import args

def seed_config():
    """ 设置随机种子和运算device设备 """
    seed = args.seed
    gpu_num = args.gpu

    random.seed(seed)
    np.random.seed(seed)

    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    # torch.set_num_threads(args.thread)  # 设置pytorch并行线程数
    if torch.cuda.is_available() and gpu_num >= 0:
        torch.backends.cudnn.benchmark = False  # type: ignore
        torch.backends.cudnn.deterministic = True  # type: ignore

        device = torch.device("cuda:" + str(gpu_num))
    else:
        device = torch.device("cpu")

    args.device = device    # 设置运算device设备


seed_config()
