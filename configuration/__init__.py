"""
该package在引入时自动自行包内所有模块，
生成args参数并对其进行设置，最后再打印出来
"""

import configuration.args_config # 生成args参数

import configuration.seed_config # 设置device及seed
import configuration.env_config  # 设置环境的最小动作

import configuration.log_config  # 最后执行打印语句，以便args参数完全初始化