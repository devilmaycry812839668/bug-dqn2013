import random
import argparse

import atari_py


parser = argparse.ArgumentParser(description="DQN2013")

# configuration for files and dirs
parser.add_argument("--exp_name", type=str, default="exp", help="Experiment BaseName")
parser.add_argument(
    "--game",
    type=str,
    default="breakout",
    choices=atari_py.list_games(),
    help="ATARI game",
)
parser.add_argument(
    "--seed", type=int, default=random.randrange(1000000), help="Random seed"
)

# configuration for torch
parser.add_argument("--gpu", type=int, default=-1, help="GPU device")
parser.add_argument("--thread", type=int, default=8, help="Number of threads")

# configuration for env
parser.add_argument(
    "--history-length",
    type=int,
    default=4,
    help="Number of consecutive states processed",
)
parser.add_argument(
    "--max-episode-length",
    type=int,
    default=int(18e3),
    help="Max episode length in game frames (0 to disable)",
)
parser.add_argument(
    "--action-repeat", type=int, default=4, help="Action repetition times"
)

# configuration for replay
parser.add_argument(
    "--memory-capacity",
    type=int,
    default=int(1e6),
    help="Experience replay memory capacity",
)
parser.add_argument("--batch-size", type=int, default=32, help="Batch size")

# configuration for model
parser.add_argument("--hidden-size", type=int, default=256, help="Network hidden size")

# configuration for exploring
parser.add_argument(
    "--eps-start", type=float, default=1, help="epsilon initialization value"
)
parser.add_argument("--eps-final", type=float, default=0.1, help="epsilon final value")
parser.add_argument(
    "--anneal-steps", type=int, default=int(1e6), help="Number of linear anneal steps"
)

# configuration for reward discount
parser.add_argument("--discount", type=float, default=0.99, help="Discount factor")

# configuration for optimizer
parser.add_argument("--lr", type=float, default=0.005, help="Learning rate")

# configuration for training
parser.add_argument(
    "--T-max",
    type=int,
    default=int(10e6),
    help="Number of training steps (4x number of frames)",
)
parser.add_argument(
    "--learn-start",
    type=int,
    default=50000,
    help="Number of steps before starting training",
)
parser.add_argument(
    "--learn-frequency",
    type=int,
    default=1,
    help="Frequency of sampling from memory to update",
)

# configuration for evaluation
parser.add_argument(
    "--fixed-states-size",
    type=int,
    default=500,
    help="Number of states for evaluating mean maxQ",
)
parser.add_argument(
    "--evaluation-interval",
    type=int,
    default=int(1e5),
    help="Number of training steps between per evaluation",
)
parser.add_argument(
    "--steps-per-evaluation",
    type=int,
    default=int(1e4),
    metavar="N",
    help="Number of Steps for evaluating mean Q per evaluation",
)
parser.add_argument(
    "--render", action="store_true", help="Display screen (testing only)"
)


args = parser.parse_args()
args.debug = False     # args.__dict__["debug"] = False
