import cv2
import random
import numpy as np
import atari_py
from collections import deque

from configuration.args_config import args


class ALE():
    def __init__(self):
        self.history_length = args.history_length
        self.action_repeat = args.action_repeat

        self.ale = atari_py.ALEInterface()
        self.ale.loadROM(atari_py.get_game_path(args.game))

        self.ale.setInt('random_seed', args.seed)
        self.ale.setInt('max_num_frames_per_episode', args.max_episode_length)
        self.ale.setFloat('repeat_action_probability', 0)  # 可变更的地方

        self.lives = self.ale.lives()
        self.life_termination = False
        self.state_buffer = deque([], maxlen=args.history_length)

        actions = self.ale.getMinimalActionSet()
        self.actions = dict()
        for idx, act in enumerate(actions):
            self.actions[idx] = act
        self.action_space = len(self.actions)

        args.action_space = self.action_space


    def _resize_state(self):
        state = cv2.resize(
            self.ale.getScreenGrayscale(),
            (84, 110),
            interpolation=cv2.INTER_LINEAR)
        state = state[26:, :]  # 84 * 84
        return state

    def step(self, action):
        reward, done = 0, False
        frame_buffer = np.zeros((2, 84, 84), dtype=np.uint8)

        for t in range(self.action_repeat):
            reward += self.ale.act(self.actions[action])
            frame_buffer[t % 2] = self._resize_state()
            done = self.ale.game_over()

            lives = self.ale.lives()
            if lives < self.lives:
                if lives > 0:
                    self.life_termination = True
                done = True
            if done:
                break

        observation = frame_buffer.max(0)
        self.state_buffer.append(observation)

        if reward != 0:
            reward = reward // abs(reward)

        return np.stack(self.state_buffer), reward, done

    def reset(self):
        for _ in range(self.history_length):
            self.state_buffer.append(np.zeros((84, 84), dtype=np.uint8))

        if not self.life_termination:
            self.ale.reset_game()
        self.lives = self.ale.lives()

        n = random.randint(1, 30)
        for _ in range(n):  # 随机初始化状态
            self.ale.act(0)
            if args.debug == True:
                print("." * 50, n)
            if self.ale.game_over() or (self.ale.lives() < self.lives):
                self.ale.reset_game()
                if args.debug == True:
                    print("."*50, "     reset")
                self.ale.act(0)
                self.lives = self.ale.lives()
                break

        self.life_termination = False

        observation = self._resize_state()
        self.state_buffer.append(observation)

        return np.stack(self.state_buffer)

    def render(self):
        cv2.imshow('screen', self.ale.getScreenRGB()[:, :, ::-1]) # x86等小端CPU运行为BGR格式
        cv2.waitKey(1)

    def close(self):
        cv2.destroyAllWindows()

