# encoding:UTF-8
"""Code from https://github.com/tambetm/simple_dqn/blob/master/src/replay_memory.py"""

import random
import numpy as np

from configuration.args_config import args


class Replay(object):
    #def __init__(self, args):
    def __init__(self):
        self.capacity = args.memory_capacity  # 缓存池的总容量
        #self.dims = (args.screen_height, args.screen_width)  # 一帧图像的高、宽
        self.dims = (84, 84)  # 一帧图像的高、宽
        self.history_length = args.history_length  # 一个状态，state的历史数据长度
        #self.n = args.multi_steps  # multi steps 步数
        self.n = 1  # multi steps 步数
        self.discount = args.discount  # reward的折扣率
        # 判断设置是否正确
        assert self.history_length >= 1  # history_length，状态state由几帧图像组成，大小至少为1
        assert self.n >= 1

        self.index = 0  # 指针指向的索引号，下一帧数据插入的位置
        self.total = 0  # buffer中已填充的个数
        self.reward_n_step_scaling = np.array([self.discount ** i for i in range(self.n)])

        """ replay buffer  定义经验池 s,a,r,s_next,terminal """
        self.states = np.empty((self.capacity,) + self.dims, dtype=np.uint8)
        self.actions = np.empty(self.capacity, dtype=np.uint8)
        self.rewards = np.empty(self.capacity, dtype=np.float32)
        self.non_terminals = np.empty(self.capacity, dtype=np.uint8)

    def append(self, state, action, reward, terminal):
        assert state.shape == self.dims  # 判断传入的游戏画面维度是否符合设定
        self.states[self.index, ...] = state
        self.actions[self.index] = action
        self.rewards[self.index] = reward
        self.non_terminals[self.index] = not terminal

        self.total = max(self.total, self.index + 1)  # 当前buffer中现有存储数据的大小
        # 加入新值后，指针位置自动加一
        self.index = (self.index + 1) % self.capacity

    def _get_samples(self, index_array):
        all_states = self.states[index_array, ...]
        all_actions = self.actions[index_array]
        all_rewards = self.rewards[index_array]
        all_non_terminals = self.non_terminals[index_array]

        ### s, s_next
        states = all_states[:, :self.history_length, ...]
        next_states = all_states[:, self.n:self.n + self.history_length, ...]

        ### a
        actions = all_actions[:, self.history_length - 1]
        actions = np.asarray(actions, dtype=np.int32)    # pytorch要求索引向量为int32类型

        ### r
        _rewards = all_rewards[:, self.history_length - 1: -1]
        rewards = np.matmul(_rewards, self.reward_n_step_scaling)

        ### non_terminals
        non_terminals = all_non_terminals[:, self.history_length + self.n - 1]

        return states, actions, rewards, next_states, non_terminals

    def sample(self, batch_size):
        assert self.total > self.history_length + self.n  # 最小容量大于一次抽样的数据大小
        # sample random indexes
        idxes = []

        if self.total == self.capacity:  ### full
            while len(idxes) < batch_size:
                idx = random.randint(0, self.capacity - 1)
                if (self.index - idx) % self.capacity > self.n and \
                        (idx - self.index) % self.capacity >= self.history_length - 1:
                    ab = np.arange(idx - self.history_length + 1, idx + self.n + 1) % self.capacity
                    cd = ab[:-1]
                    if np.any(self.non_terminals[cd] == 0):
                        continue
                    else:
                        idxes.append(ab)
        else:  ### not full
            while len(idxes) < batch_size:
                idx = random.randint(self.history_length - 1, self.index - 1 - self.n)
                ab = np.arange(idx - self.history_length + 1, idx + self.n + 1)
                cd = ab[:-1]
                if np.any(self.non_terminals[cd] == 0):
                    continue
                else:
                    idxes.append(ab)

        idxes = np.asarray(idxes)

        return self._get_samples(idxes)
