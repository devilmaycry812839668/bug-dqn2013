import numpy as np

from configuration.args_config import args
from environment.atari_env import ALE
from backend import (
    get_dqn_model,
    get_loss_fun,
    get_optimizer_fun,
    update_gradients,

    get_maxQ_action_index,
    get_actQ_value,
    get_maxQ_value,
    get_target_Q_value,

    get_mean_maxQ_value,
    save_model,
)


class Agent:
    def __init__(self):
        self.dqn = get_dqn_model()
        self.optimizer = get_optimizer_fun(self.dqn)

        self.loss_fun = get_loss_fun()
        self.update_gradients = update_gradients

        self.n = 1
        self.discount = args.discount
        self.action_space = args.action_space


    # Acts based on single state
    def act_max(self, state):
        return get_maxQ_action_index(self.dqn, state)

    # Acts based on single state, with an ε-greedy policy
    def act_e_greedy(self, state, epsilon=0.05):
        if np.random.random() < epsilon:
            return np.random.randint(0, self.action_space)
        else:
            return self.act_max(state)


    def learn(self, batch):
        states, actions, returns, next_states, nonterminals = batch

        Q_value = get_actQ_value(self.dqn, states, actions)
        next_Q_value = get_maxQ_value(self.dqn, next_states)

        discount = self.discount**self.n
        target_Q_value = get_target_Q_value(returns, nonterminals, discount, next_Q_value)

        loss = self.loss_fun(Q_value, target_Q_value)
        self.update_gradients(loss, self.optimizer)

        return loss.detach().numpy()      # 标量，torch类型转为numpy类型

    # Evaluates max Q-value for a batch of state
    def evaluate_mean_maxQ_value(self, states):
        return get_mean_maxQ_value(self.dqn, states)
        

    # Save model parameters on current device
    def save(self, path, number):
        name = str(number) + "_model.pth"
        save_model(self.dqn, path, name)

